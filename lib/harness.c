#include<stdio.h>
#include<stdlib.h>
#include<stdint.h>
#include<string.h>
#include<limits.h>
#include<unistd.h>
#include<stdbool.h>


typedef uint8_t  u8;
typedef uint16_t u16;
typedef uint32_t u32;

typedef int8_t   s8;
typedef int16_t  s16;
typedef int32_t  s32;
typedef int64_t  s64;
typedef unsigned long sector_t;

#ifdef __x86_64__
typedef unsigned long long u64;
#else
typedef uint64_t u64;
#endif /* ^__x86_64__ */



/* Variables used in the routine */
static char *fp  = NULL;
static short state = -1;
static int   inplen = 0;
static int   cur_pos = 0;

static char buf[2048];


static void getInput(){
  inplen = read(STDIN_FILENO,buf,sizeof(buf));
  if(inplen > 0){
    fp = buf;
    state = 0;
#ifdef DEBUG
    printf("Got the input string of length = %d\n",inplen);
#endif
  }else{
#ifdef DEBUG
    printf("Unable to read the input stream \n");
#endif
    abort();
  }
}


int __VERIFIER_nondet_int(void){
  int  *ifp = NULL;
  int val;
  if(state == -1) getInput();
  if(state == 0){
    if((fp + sizeof(int)) - buf <= inplen){
      ifp = (int *)fp;
      fp += sizeof(int);
      cur_pos +=sizeof(int);
      val = *ifp;
#ifdef DEBUG
      printf("Got the intput int : %d\n",val);
#endif
    }else{
      state = 1; // Endof input reached
      val = 0;
    }
  }else{
#ifdef DEBUG
    printf("Reading beyond input \n");
#endif
    val = 0 ;
  }
  return val;
}




void __VERIFIER_error(int errorCodeID){
  
  printf("\nError Code Reached - %d\n",errorCodeID) ;
#ifdef DEBUG
  printf("Hitting the error..@len %d Hurrey !!!!\n",cur_pos);
#endif
  /* It is assumed that the output dir name is available
     There is no explicit checking is done on the presence
     crash directory or not.
  */
  char *crash_file;
  char *crash_dir = getenv("CRASH_DIR");
  if(crash_dir){
    int flen = snprintf(NULL,0,"%s",crash_dir) + strlen("crash_001") + 2;
    crash_file = (char *)calloc(flen,sizeof(char));
  }else{
    crash_file = (char *)calloc(strlen("./crash_001")+1,sizeof(char));
    crash_dir = ".";
  }
  if(crash_file){
    sprintf(crash_file,"%s/%s",crash_dir,"crash_001");
#ifdef DEBUG
    printf("Crash file is %s \n",crash_file);
#endif
    FILE *fc         = fopen(crash_file,"wb");
    free(crash_file);
    if(!fc){
#ifdef DEBUG
      printf("Crash file %s does not exist !!!\n",crash_file);
#endif
      abort();
    }else{
      fwrite(buf,sizeof(char),cur_pos > inplen ? inplen : cur_pos,fc);
      fclose(fc);
    }
  }
  abort();
}

void __VERIFIER_assume(int expression){
{
  if (!expression) {
#ifdef DEBUG
    printf("Assume failed %d \n",expression);
#endif
    exit(128);}
    //  LOOP: goto LOOP; };
#ifdef DEBUG
  printf("Assume passed %d \n",expression);
#endif
  return; }
}
