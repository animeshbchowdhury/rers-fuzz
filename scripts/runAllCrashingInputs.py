#!/usr/bin/python3

import re,os,shutil
import argparse
exePath = ""
crashDir = ""


def setAppropriatePaths(cmdArgs):
    global exePath,crashDir
    exePath = cmdArgs.exe
    crashDir = cmdArgs.crash
    if(not os.path.exists(exePath)):
        print("exe path doesn't exist")
        exit(1)
    if (not os.path.exists(crashDir)):
        print("Crash Dir doesn't exist")
        exit(1)

def runTestcase(TEST_NAME):
    witnessRunCmd = exePath + " < " + TEST_NAME + " > run.log 2>&1 "
    #print(witnessRunCmd)
    os.system(witnessRunCmd)
    logFilePath = os.getcwd()+os.sep+"run.log"
    readRunLog = open(logFilePath,'r')
    readLinesOfLog = readRunLog.readlines()
    for readLine in readLinesOfLog:
        if readLine.__contains__("Error Code"):
            print(readLine)
    readRunLog.close()
    os.remove(logFilePath)


def checkCrashTestDirectory():
    testcaseDir = crashDir
    if(not os.path.exists(testcaseDir)):
        return

    testList = os.listdir(testcaseDir)
    testList.sort(reverse=True)
    for t in testList:
        testFile = testcaseDir + os.sep + t
        if (t.startswith("id:")):
            #print(t)
            runTestcase(testFile)

def processCmdLineArguments():
    parser = argparse.ArgumentParser(prog='crashEvaluator', description="Crash Eval Tool")
    parser.add_argument('--exe', required=True, help="Program Exectuable")
    parser.add_argument('--crash', required=True, help="Crash Dir Path")
    return parser.parse_args()

def main():
    cmdArgs = processCmdLineArguments()
    setAppropriatePaths(cmdArgs)
    checkCrashTestDirectory()

if __name__ == '__main__':
    main()
