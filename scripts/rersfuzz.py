#!/usr/bin/python3
'''
   rersFuzz Top level Script : Setting Appropriate Program parameters
   --------------------------------------------------------------------
   Written by Animesh Basak Chowdhury
   Licensed under BSD 4 clause License

'''

import argparse
import os
import shutil, time

rersFuzz_ROOT = ""
rersFuzz_TEMP = ""
DICTIONARY = ""
HOME_DIR = ""

APPLICATION_EXECUTABLE = ""
INPUT_SEED = ""
rersFuzz_SCRIPTS = ""
C_FILE = ""
C_FILE_SRC = ""

def printHeaderContent():
    global rersFuzz_ROOT
    print("\n\n")
    print("**************** rersFuzz RUN STARTED ****************** ")
    print("\n\n")
    versionInfoFile = rersFuzz_ROOT+os.sep+"supportFiles"+os.sep+"versionInfo.txt"
    if(os.path.isfile(versionInfoFile)):
        displayCommand = "cat "+versionInfoFile
        print("Version - ")
        os.system(displayCommand)
    else:
        exitMessage = "Version Info File Not FOUND."
        exitContent(exitMessage,1)

def exitContent(exitMessage,exitCode):
    print("\n" + exitMessage + "\n")
    exit(exitCode)

def printContent(logWord):
    print("\n"+logWord+"...  PASS")

def setPATHSandENVvars(cmdArgs):
    global HOME_DIR, rersFuzz_TEMP, rersFuzz_ROOT,rersFuzz_SCRIPTS,C_FILE,C_FILE_SRC,DICTIONARY


    if (not os.path.exists(cmdArgs.cfile)):
        exitMessage = "C file doesn't exist. Please check the path and rerun with Appropriate Arguements."
        exitContent(exitMessage, 1)

    HOME_DIR = os.environ["HOME"]
    rersFuzz_TEMP = HOME_DIR + os.sep + "rersFuzz_TEMP_DIR"
    rersFuzz_SCRIPT_DIR = os.path.split(os.path.abspath(__file__))[0]
    rersFuzz_ROOT = os.path.split(rersFuzz_SCRIPT_DIR)[0]
    rersFuzz_SCRIPTS = rersFuzz_ROOT+os.sep+"scripts"
    C_FILE_SRC = cmdArgs.cfile
    C_FILE = rersFuzz_TEMP + os.sep + C_FILE_SRC.split(os.sep)[-1]

    setAppropriateENVvariables(rersFuzz_ROOT, rersFuzz_TEMP)

    if (cmdArgs.dictTokens != None):
        if (not os.path.exists(cmdArgs.dictTokens)):
            exitMessage = "Dictionary file doesn't exist. Please check the path and rerun with Appropriate Arguements."
            exitContent(exitMessage, 1)
        else:
            DICTIONARY = cmdArgs.dictTokens
    else:
        DICTIONARY = rersFuzz_ROOT+os.sep+"fuzzEngine"+os.sep+"dictionaries"+os.sep+"industrial_token.dict"

    printHeaderContent()
    logWord = "Environment Variables set"
    printContent(logWord)

def setAppropriateENVvariables(vroot,vtemp):
    os.environ["rersFuzz_ROOT"] = vroot
    os.environ["rersFuzz_TEMP"] = vtemp

def createrersFuzzTempDir():
    global rersFuzz_ROOT,rersFuzz_TEMP
    if os.path.exists(rersFuzz_TEMP):
        shutil.rmtree(rersFuzz_TEMP)
    os.mkdir(rersFuzz_TEMP)
    logWord = "rersFuzz TEMP DIRECTORY created"
    printContent(logWord)

def copyAppropriateFilesToTempDir():
    createrersFuzzTempDir()
    shutil.copy(C_FILE_SRC,C_FILE)

    logWord = "C file copied to working folder"
    printContent(logWord)

def processCommandLineArguements():
    parser = argparse.ArgumentParser(prog='rersFuzz', description="rersFuzz Tool")
    parser.add_argument('--version', action='version', version='1.0.0')
    parser.add_argument('--cfile', required=True,help="Absolute Path of C File")
    parser.add_argument('--dictTokens', required=False,help="Token/Dictionary file")

    return parser.parse_args()

def examineRetCode(retCode):
    exitCode = retCode >> 8
    if (exitCode == 1):
        message = "rersFuzz Terminated Abruptly... Please check the logs"
        exitContent(message,1)

def runrersFuzz():
    currentWD = os.getcwd()
    os.chdir(os.environ["rersFuzz_TEMP"])
    fuzzEngineExecutable = os.environ["rersFuzz_ROOT"]+os.sep+"fuzzEngine"+os.sep+"rers-fuzz"
    inputSeedDir = os.environ["rersFuzz_ROOT"]+os.sep+"seeds"
    aflOutputDir = os.environ["rersFuzz_TEMP"]+os.sep+"fuzzOutputDir"

    fuzzEngineRunCmd = " AFL_SKIP_CPUFREQ=1 "+fuzzEngineExecutable+ " -i "+inputSeedDir+" -o "+aflOutputDir+\
                       " -e -x "+ DICTIONARY + " -- " + APPLICATION_EXECUTABLE

    print("\n\nFUZZ ENGINE RUNNING ... ")
    print(fuzzEngineRunCmd)
    print("\n\n")
    os.system(fuzzEngineRunCmd)

def prepareBenchmarkExecutable():
    global APPLICATION_EXECUTABLE
    currentWD = os.getcwd()
    os.chdir(os.environ["rersFuzz_TEMP"])

    compiler = os.environ["rersFuzz_ROOT"]+os.sep+"fuzzEngine"+os.sep+"afl-clang-fast"
    harnessFile = os.environ["rersFuzz_ROOT"]+os.sep+"lib"+os.sep+"harness.c"
    APPLICATION_EXECUTABLE = os.environ["rersFuzz_TEMP"] + os.sep + "instrumentedProgramExec"

    compilationCommand = "MARKSET=1"+" "+compiler+" "+C_FILE+" "+harnessFile+" -o "+APPLICATION_EXECUTABLE

    print("\n\nCompilation Command - \n")
    printContent(compilationCommand)

    os.system(compilationCommand)
    os.chdir(currentWD)


def main():
    global startTime
    startTime = time.time()
    cmdArgs = processCommandLineArguements()
    setPATHSandENVvars(cmdArgs)
    copyAppropriateFilesToTempDir()
    prepareBenchmarkExecutable()
    runrersFuzz()

if __name__ == "__main__":
    main()