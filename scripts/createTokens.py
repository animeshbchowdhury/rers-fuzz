tokenTail = "\\x00\\x00\\x00"
tokenHead = "\\x"

print(tokenTail)
print(tokenHead)

start = 55
end   = 127

#print(str(hex(48)).split("x")[1])


tokenDictFile = "industrial_token_"+str(start)+"_to_"+str(end)+".dict"
dictFileHandler = open(tokenDictFile,'w+')

for i in range(start,end+2):
    dictWord = "input"+str(i)+"=\""+tokenHead+str(hex(i)).split("x")[1]+tokenTail+"\"\n"
    dictFileHandler.write(dictWord)
    #print(str(hex(55)).strip("0x"))

dictFileHandler.close()
